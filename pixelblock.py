#!/bin/env python3
# Gets block/namespace id and images k-mean to single color value
# and stored in a color configuration file

import io
import json
import sys

from bs4 import BeautifulSoup
from six.moves import urllib
from PIL import Image
import numpy as np
import scipy
import cv2
import sklearn.cluster


wiki = "https://minecraft.fandom.com"

# Round each value and turn into integers
def roundList(values):
	return [int(round(a)) for a in values]

# Exponent alpha
def expAlpha(values):
	if len(values) < 4:
		return values
	if values[3] == 255:
		return values
	values[3] = int(((values[3] / 255) ** 4) * 255)
	return values

# Download an image and return its bytes
def downloadImage(url):
	with urllib.request.urlopen(url) as fp:
		return io.BytesIO(fp.read())
	return None

#https://stackoverflow.com/a/3244061
def dominantImage(image):
	NUM_CLUSTERS = 5

	if image == None:
		return [0, 0, 0, 0]

	# Open the image
	img = Image.open(image).convert('RGBA')

	ar = np.asarray(img)
	shape = ar.shape
	if len(shape) < 3:
		img.close()
		image.seek(0)
		color = getMean(image)
		return color
	# Get rid of all transparent colors
	ar = ar[ar[:,:,3]!=0][:,:]

	# No color left, average it instead
	if ar.shape[0] == 0:
		img.close()
		image.seek(0)
		color = getMean(image)
		return color

	# Convert to proper format
	ar = ar.reshape(ar.shape[0], ar.shape[1]).astype(float)

	# K-mean the image
	codes, _ = scipy.cluster.vq.kmeans(ar, NUM_CLUSTERS)

	# Assign codes
	vecs, _ = scipy.cluster.vq.vq(ar, codes)

	# Count occurrences
	counts, bins = np.histogram(vecs, len(codes))

	# Find most frequent
	index_max = np.argmax(counts)
	peak = codes[index_max]

	# Handle grayscale
	if len(peak) == 2:
		peak = [peak[0], peak[0], peak[0], peak[1]]
	elif len(peak) == 1:
		peak = [peak[0], peak[0], peak[0]]

	# Get as RGBA
	return roundList(peak)

# Get dominant color through k-mean
def getDominant(url):
	content = downloadImage(url)

	return dominantImage(content)

# Get mean color
#https://stackoverflow.com/a/43111221
def getMean(content):
	file_bytes = np.asarray(bytearray(content.read()), dtype=np.uint8)
	img = cv2.imdecode(file_bytes, cv2.IMREAD_COLOR)
	average = img.mean(axis=0).mean(axis=0)
	return roundList(average)

# Download all namespace id from minecraft wiki
def downloadNamespaceIDs():
	url = wiki + "/api.php?action=parse&format=json&prop=text%7Cmodules%7Cjsconfigvars&title=Java_Edition_data_values&text=%7B%7B%3AJava%20Edition%20data%20values%2FBlocks%7D%7D"
	content = urllib.request.urlopen(url).read()

	content = json.loads(content)["parse"]["text"]['*']

	soup = BeautifulSoup(content, features='html.parser')

	start = soup.find(lambda tag: tag.name == "th" and tag.string.strip() == "Block")

	if start == None:
		return None

	table = start.find_parent("table")

	rows = table.findAll("tr")[1:]

	def parseRow(row):
		name = row.find("code").string.strip()
		img = row.find("img")
		if img != None:
			a = img.find_parent('a')
			if img.parent == a:
				img = a["href"]
			else:
				img = img["src"]
		return (img, ["minecraft:" + name])

	return [parseRow(row) for row in rows]

# Download all block id from minecraft wiki
def downloadBlockIDs():
	url = wiki + "/wiki/Java_Edition_data_values/Pre-flattening/Block_IDs"
	content = urllib.request.urlopen(url).read();

	soup = BeautifulSoup(content, features='html.parser')

	start = soup.findAll(lambda tag: tag.name == "th" and tag.string.strip() == "Block")

	if start == None or len(start) == 0:
		return None

	tables = [col.find_parent("table") for col in start]

	rows = [table.findAll("tr")[1:] for table in tables]
	# Flatten
	rows = [row for sublist in rows for row in sublist]

	def parseRow(row):
		cols = row.findAll("td")
		name = cols[3].string.strip()
		bid = int(cols[1].find("span").string.strip()) if cols[1].string == None else int(cols[1].string.strip())
		img = cols[0].find("img")
		if img != None:
			a = img.find_parent('a')
			if img.parent == a:
				img = a["href"]
			else:
				img = img["src"]
		names = (["minecraft:" + name] if name else []) + [bid]
		return (img, names)

	return [parseRow(row) for row in rows]

# Get a specific full image from a link
def getFullImageLink(url):
	content = urllib.request.urlopen(url).read()

	soup = BeautifulSoup(content, features='html.parser')

	div = soup.find("div", {"id": "file"})

	if not div:
		return None

	return div.find("a")["href"]

# Simple progress bar to be displayed
def progressBar(n, m):
	sys.stdout.write("\r{} / {}".format(n, m))
	sys.stdout.flush()
	if n == m:
		sys.stdout.write("\n")

# Get ids
print("Retrieving Block IDs...")

oldValues = downloadBlockIDs()

if not oldValues:
	exit("Block ID page have changed")

print("Got", len(oldValues), "entries.")

print("Retrieving Namespace IDs...")

newValues = downloadNamespaceIDs()

if not newValues:
	exit("Namespace ID page have changed")

print("Got", len(newValues), "entries.")

# Merge lists
print("Merging lists...")

values = dict()

for key, val in newValues:
	if key in values:
		values[key] += val
	else:
		values[key] = val

for key, val in oldValues:
	if key in values:
		values[key] += val
	else:
		values[key] = val

# Save to blockcolor.conf
print("Saving", len(values), "blocks...")

with open("blockcolor.conf", "w") as f:
	print("# Auto generated by PixelBlock", file=f)
	n = 0
	m = len(values)
	for key in values:
		ids = values[key]
		url = key
		color = None
		if url != None:
			color = getDominant(url)
		if color == None:
			color = [0, 0, 0, 0]
		print(' '.join([str(a) for a in ids]), "=", ' '.join([str(a) for a in color]), file=f)
		n += 1
		progressBar(n, m)

