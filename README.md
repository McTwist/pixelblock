# PixelBlock

Created to generate blockcolor.conf, which contains all block and namespace ids and respective color. 

Uses Minecraft Wiki to attain values. Use whenever it has been updated with new values and images.

This should only be used as a template to be modified to desired result.

*DISCLAIMER: This works like a scraper but is not optimized as one. Do not use carelessly.*

## Installation

Requires to install some additional python 3 packages:

```bash
pip3 install -r requirements.txt
```
